from django.contrib import admin
from django.urls import path, include
from rest_framework import routers, serializers, viewsets

from .models import Group, User, Category, Quiz, Question, Option, Comment, Submission, Result

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    group = GroupSerializer()
    class Meta:
        model = User
        fields = '__all__'

class QuizSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    class Meta:
        model = Quiz
        fields = '__all__'

class QuestionSerializer(serializers.ModelSerializer):
    quiz = QuizSerializer()
    class Meta:
        model = Question
        fields = '__all__'

class OptionSerializer(serializers.ModelSerializer):
    question = QuestionSerializer()
    class Meta:
        model = Option
        fields = '__all__'

class CommentSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    quiz = QuizSerializer()
    class Meta:
        model = Comment
        fields = '__all__'

class SubmissionSerializer(serializers.ModelSerializer):
    quiz = QuizSerializer()
    class Meta:
        model = Submission
        fields = '__all__'

class ResultSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    submission = SubmissionSerializer()
    question = QuestionSerializer()
    answer = OptionSerializer()
    class Meta:
        model = Result
        fields = '__all__'

class ResultsViewSet(viewsets.ModelViewSet):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer

class QuestionsViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class QuizesViewSet(viewsets.ModelViewSet):
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


router = routers.DefaultRouter()

router.register(r'results', ResultsViewSet)
router.register(r'questions', QuestionsViewSet)
router.register(r'quizes', QuizesViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('', admin.site.urls)
]