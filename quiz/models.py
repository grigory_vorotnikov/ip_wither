from django.db import models
from django.utils.timezone import now

class Group(models.Model):
    title = models.CharField(max_length=7)
    course = models.PositiveSmallIntegerField(default=1)
    def __str__(self):
        return f"{self.title}"

class User(models.Model):
    name = models.CharField(max_length=70)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    points = models.IntegerField()
    created_at = models.DateTimeField(auto_created=True)
    def __str__(self):
        return f"{self.name}"

class Category(models.Model):
    title = models.CharField(max_length=50)
    def __str__(self):
        return f"{self.title}"

class Quiz(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=100)
    duration = models.IntegerField(default=0)
    def __str__(self):
        return f"{self.category}: {self.title}"

class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=200)
    image_url = models.CharField(max_length=1024, null=True, blank=True)
    weight = models.FloatField(default=1)
    def __str__(self):
        return f"{self.title}"

class Option(models.Model):
    title = models.CharField(max_length=200)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    image_url = models.CharField(max_length=1024, null=True, blank=True)
    is_right = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.title}"

class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    body = models.TextField(blank=True)
    reply_to = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    created = models.DateTimeField(auto_created=True, default=now)
    def __str__(self):
        return f"{self.author}: {self.body}"

class Submission(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    started = models.DateTimeField(default=now)
    finished = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f"{self.quiz}, {self.finished}"

class Result(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.ForeignKey(Option, on_delete=models.CASCADE)
    points = models.IntegerField(default=0)
    answered = models.DateTimeField(default=now)
    def __str__(self):
        return f"{self.user}, {self.question}, {self.answer}"