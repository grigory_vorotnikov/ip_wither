from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.html import format_html
from .models import Question, Quiz, Option, Comment, Submission, Category, User, Group, Result

def make_right(modeladmin, request, queryset):
    queryset.update(is_right=True)
def make_is_not_right(modeladmin, request, queryset):
    queryset.update(is_right=False)

class UserResource(resources.ModelResource):
    class Meta:
        model = User

class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question

class QuizResource(resources.ModelResource):
    class Meta:
        model = Quiz

class OptionResource(resources.ModelResource):
    class Meta:
        model = Option

class CommentResource(resources.ModelResource):
    class Meta:
        model = Comment

class SubmissionResource(resources.ModelResource):
    class Meta:
        model = Submission

class GroupResource(resources.ModelResource):
    class Meta:
        model = Group

class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category

class ResultResource(resources.ModelResource):
    class Meta:
        model = Result

@admin.register(User)
class UserAdmin(ImportExportModelAdmin):
    resource_class = UserResource
    list_display = ['name', 'group','points', 'created_at']

@admin.register(Group)
class GroupAdmin(ImportExportModelAdmin):
    resource_class = GroupResource
    list_display = ['title', 'course']

@admin.register(Quiz)
class QuizAdmin(ImportExportModelAdmin):
    resource_class = QuizResource
    list_display = ['category', 'title', 'duration']

@admin.register(Question)
class QuestionAdmin(ImportExportModelAdmin):
    resource_class = QuestionResource
    list_display = ['quiz','title','image_url','weight']

@admin.register(Option)
class OptionAdmin(ImportExportModelAdmin):
    resource_class = OptionResource
    actions=[make_right, make_is_not_right]
    list_display = ['title','question','image_url','is_right']

@admin.register(Comment)
class CommentAdmin(ImportExportModelAdmin):
    resource_class = CommentResource
    list_display = ['author','quiz','body','reply_to','created']

@admin.register(Submission)
class SubmissionAdmin(ImportExportModelAdmin):
    resource_class = SubmissionResource
    list_display = ['quiz', 'started', 'finished']

@admin.register(Category)
class CategoryAdmin(ImportExportModelAdmin):
    resource_class = CategoryResource
    list_display = ['title']

@admin.register(Result)
class ResultAdmin(ImportExportModelAdmin):
    resource_class = ResultResource
    list_display = ['user', 'submission', 'question', 'answer', 'points','answered']